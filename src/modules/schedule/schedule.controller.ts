import {
  BadRequestException,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param
} from '@nestjs/common';
import {ScheduleService} from './schedule.service';
import {IScheduleItem} from "./types";


@Controller('schedule')
export class ScheduleController {
  constructor(
    protected readonly service: ScheduleService,
  ) {}



  seasons = ['autumn','spring', 'summer']

  @Get(':year/:season')
  async getSchedule(@Param('year') year: number, @Param('season') season: string): Promise<IScheduleItem[]> {
    if (!this.seasons.includes(season)) {
      throw new BadRequestException(`Season ${season} is not valid`);
    }

    const scheduleItems = await this.service.getSchedule(year, season);
    if (scheduleItems.length === 0) {
      throw new NotFoundException('No schedule found');
    }
    return scheduleItems;
  }

}
