import {Injectable, NotFoundException} from '@nestjs/common';
import { EducationLevel, CourseSeason } from '../../common/types';
import {IScheduleItem} from "./types";
import axios from 'axios';
import { load } from 'cheerio'
import * as https from "https";

@Injectable()
export class ScheduleService {

    async getSchedule(year: number, season: string): Promise<IScheduleItem[]> {
    const httpsAgent = new https.Agent({rejectUnauthorized: false});
    let realYear = year;
    let seasonNumber: number;
    let courseSeason: CourseSeason;

    switch (season) {
      case 'autumn':
        seasonNumber = 1;
        courseSeason = CourseSeason.AUTUMN;
        break;
      case 'spring':
        seasonNumber = 2;
        courseSeason = CourseSeason.SPRING;
        realYear--;
        break;
      case 'summer':
        seasonNumber = 3;
        courseSeason = CourseSeason.SUMMER;
        realYear--;
        break;
      default:
        throw new NotFoundException(`Season ${season} not exists`);
    }

    const response = await axios.get(`https://my.ukma.edu.ua/schedule/?year=${realYear}&season=${seasonNumber}`, {httpsAgent});
    const data = response.data;

    return this.normalizeData(data, courseSeason);
  }

  private normalizeData(data: string, courseSeason: CourseSeason): IScheduleItem[] {
        const schedules: IScheduleItem[] = [];
        const $ = load(data);
      
        $('#schedule-accordion > div').each((i, panel) => {
            const faculty = $(panel).find("div").eq(0).find("a").text().trim();
            const facultyBody = $(panel).find("div").eq(1).find("div[id^='schedule-faculty'] > div");
      
          facultyBody.each((id, el) => {
            const main = $(el).find("a").text().trim().split(", ");
            const year: number = +main[1].split(" ")[0];

            const body = $(el).find("div[id^='schedule-faculty'] li > div");
            body.each((index, el) => {
                const current = $(el).find("a[title='Завантажити']").not("[target='_blank']");
                const [, date, time] = $(el).find('span').text().trim().split(' ');
                schedules.push({
                    url: current.attr('href') || "",
                    updatedAt: `${date} ${time}`,
                    facultyName: faculty,
                    specialityName:  current.text().trim(),
                    level: main[0] === 'БП' ? EducationLevel.BACHELOR : EducationLevel.MASTER,
                    year: year as 1 | 2 | 3 | 4,
                    season: courseSeason
                });
            });
          });
        });
      
        if (schedules.length === 0) {
          throw new NotFoundException('No schedule items found');
        }
      
        return schedules; 
}
}

